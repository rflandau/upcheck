package link

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"upcheck/classes/youtube"
)

// DEAD marks a link as such
const DEAD int = -100

// Link represents all the data on a link
type Link struct {
	URL        string
	Section    string
	Subsection string
	Notes      []string
	response   *http.Response
	StatusCode int
}

// constructor #################################################################

// NewLink returns a new instance of a Link struct
func NewLink(sec, subsec string) *Link {
	l := new(Link)

	l.Section = sec
	l.Subsection = subsec

	return l
}

// methods #####################################################################

// Test makes a HEAD request, then parses and returns the result.
// Response is stored in the struct.
// Does not run if link is already DEAD and !force.
// Intended to be run as a goroutine
// Returns an error or nil.
func (l *Link) Test(worker int, force bool, wg *sync.WaitGroup, yt *youtube.Client) {
	defer wg.Done()

	if l.StatusCode == DEAD && !force {
		return
	}

	// test if it is a youtube link, unless the yt parser is nil
	if yt != nil {
		l.testYT(yt)
	} else {
		l.testLink()
	}
}

func (l *Link) testYT(yt *youtube.Client) {
	// parse the URL, look for youtube
	u, err := url.ParseRequestURI(l.URL)
	if err != nil {
		l.SetDead(err.Error())
		return
	}

	// parse out ID dependent on URL style
	// TODO add support for REGEX matching against other countries YT links
	//	Such as youtube.ie, youtube.de, etc

	id, ytLink, err := yt.GetID(u)
	if err != nil {
		l.SetDead(err.Error())
		return
	} else if !ytLink { // if not yt link, call std testLink
		l.testLink()
		return
	}

	// if we made it this far, we have our id
	fmt.Println("Found video id: " + id)
	var title string
	title, err = yt.CheckStatusByID(id)
	fmt.Println("Title: ", title)
	if err != nil {
		l.SetDead(err.Error())
	} else {
		l.StatusCode = 200
	}
}

func (l *Link) testLink() {
	var err error

	l.response, err = http.Head(l.URL)
	if err == nil {
		l.StatusCode = l.response.StatusCode
	} else { // if we erred, note it in the link
		spl := strings.SplitN(err.Error(), ": ", 2) // trim off prepend (up to first colon)
		// note the error
		if len(spl) >= 2 {
			l.Notes = append(l.Notes, spl[1])
		} else {
			l.Notes = append(l.Notes, "Unknown SplitN error @ testLink")
		}
		l.StatusCode = DEAD
	}
}

func (l *Link) SetDead(note string) {
	l.Notes = append(l.Notes, note)
	l.StatusCode = DEAD
}

// Pprint method
// Pretty-prints the important struct info
func (l *Link) Pprint() string {
	ret := l.Section + ": "
	if l.Subsection != "" {
		ret += l.Subsection + ": "
	}
	ret += l.URL
	return ret
}
