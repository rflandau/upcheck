package youtube

/* Wrapper class for the youtube API.
Has a NewClient function as setup procedures are required.

Based very heavily off of the Youtube API Go quickstart:
https://developers.google.com/youtube/v3/quickstart/go */

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"golang.org/x/net/context"
	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"
)

type Client struct {
	service *youtube.Service
}

func NewClient(apiKey string) (*Client, error) {
	var c Client

	y, err := youtube.NewService(context.Background(), option.WithAPIKey(apiKey))
	if err != nil {
		return nil, err
	}
	c.service = y

	return &c, nil
}

// Methods##############################################################################################################

// GetID retrieves the ID from a Youtube URL
func (c *Client) GetID(u *url.URL) (string, bool, error) {
	h := u.Hostname()

	var id string

	if h == "www.youtube.com" || h == "youtube.com" {
		// youtube.com/embed/<ID>
		// youtube.com/watch?v=<ID>

		// try to pull id from query
		q := u.Query()
		id = q.Get("v")
		if id == "" { // if ID is empty, try embed
			spl := strings.Split(u.Path, "/")
			if len(spl) == 3 && spl[1] == "embed" {
				id = spl[2]
			}
		}
		// check if id is still empty
		if id == "" {
			return "", true, errors.New("failed to pull id from query or following 'embed/'")
		}
	} else if h == "youtu.be" {
		// youtu.be/<ID>

		// explode path on "/" and retrieve last element
		spl := strings.Split(u.Path, "/")
		// double-check format: 0: <empty> | 1: <id>
		if len(spl) == 2 && spl[0] == "" {
			id = spl[1]
		} else {
			return "", true, errors.New("failed to parse embed on explode")
		}
	} else { // not a yt link!
		return "", false, nil
	}

	// if we made it this far, we have the id
	return id, true, nil
}

// CheckStatusByID returns the status of the video and any notes.
func (c *Client) CheckStatusByID(id string) (string, error) {
	// build the request
	call := c.service.Videos.List([]string{"snippet", "statistics", "status"}).Id(id)
	// execute and check the request
	response, err := call.Do()
	if err != nil {
		return "", err
	}
	// pull the list of video from the request
	v := response.Items
	// check the number of videos returned; if != 1, we have a problem
	switch len(v) {
	case 0:
		// most issues will get caught here; the video won't even show up in the query
		return "", errors.New("no video found matching id " + id)
	case 1:

		fmt.Printf("Found video matching id %v:\n %#v\n", id, v[0])

		// we can check additional params
		http := v[0].ServerResponse.HTTPStatusCode
		if http != 0 && (http > 299 || http < 200) { // short-circuit will skip latter if response == 0
			return "", errors.New("special response code from server: " +
				strconv.Itoa(v[0].ServerResponse.HTTPStatusCode))
		}
		if v[0].Status != nil && v[0].Status.FailureReason != "" {
			return "", errors.New("other failure: " + v[0].Status.FailureReason)
		}
	default:
		return "", errors.New(strconv.Itoa(len(v)) + " videos found with id " + id + ". Unable to evaluate.")
	}

	return v[0].Snippet.Title, nil
}
