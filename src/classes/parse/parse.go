package parse

import (
	"bufio"
	"net/url"
	"os"
	"strings"
	"unicode/utf8"
	"upcheck/classes/link"
)

const (
	isJunk = iota
	isEmpty
	isSection
	isSubsection
	isURL
)

// Parser struct
type Parser struct {
	Scan *bufio.Scanner
	Out  *os.File
}

// methods #####################################################################

/* ParseFile method
Parses the given file and returns a slice of Link pointers */
func (p *Parser) ParseFile() []*link.Link {
	var section, subsection string
	var links []*link.Link

	for p.Scan.Scan() { // read a line
		status := 0
		line := strings.Trim(p.Scan.Text(), " ")
		line, lt := p.getLineType(line) // heavy-lifting done by getLineType()
		switch lt {
		case isEmpty:
			continue
		case isSection:
			section = line
		case isSubsection:
			subsection = line
		case isJunk:
			status = link.DEAD
			fallthrough
		case isURL:
			l := link.Link{
				URL:        line,
				Section:    section,
				Subsection: subsection,
				StatusCode: status}
			links = append(links, &l)
		}
	}
	return links
}

/* TrimFirstRune pulls off the first character of a string and returns the modified string.
Pulled from Cerise Limón on SO:
    https://stackoverflow.com/questions/48798588/how-do-you-remove-the-first-character-of-a-string */
func trimFirstRune(s string) string {
	_, i := utf8.DecodeRuneInString(s)
	return s[i:]
}

/* getLineType
Runs a handful of checks to figure out what kind of line we are working with and trims off marker characters. */
func (p *Parser) getLineType(line string) (string, int) {
	// if line is empty
	if line == " " || len(line) == 0 {
		return line, isEmpty
	}
	// if line is section or subsection
	if line[0] == '#' {
		line = trimFirstRune(line) // trim off #
		// if line is subsection
		if line[0] == '#' {
			line = trimFirstRune(line) // trim off second #
			return line, isSubsection
		}
		// if line is section
		return line, isSection
	}
	// if line is link
	if _, err := url.ParseRequestURI(line); err == nil {
		return line, isURL
	}

	// if we got this far, the line is junk
	return line, isJunk
}
