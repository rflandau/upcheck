module upcheck

go 1.15

require (
	cloud.google.com/go v0.82.0 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5
	golang.org/x/sys v0.0.0-20210601080250-7ecdf8ef093b // indirect
	google.golang.org/api v0.47.0
	google.golang.org/genproto v0.0.0-20210601170153-0befbe3492e2 // indirect
)
