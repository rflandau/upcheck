package main

// Note: If more robustness is required, the URL package improves URL handling

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"
	"time"
	"upcheck/classes/parse"
	"upcheck/classes/youtube"
)

// input file
var inFile string

// output file
//	if "", use STDOUT
var outFile string

// log file
//	if "", use STDOUT
var logfile string

var ytKeyFile string

// bool flags
var all bool

// init ########################################################################
// init establishes all flags
func init() {
	flag.StringVar(&inFile, "i", "", "Specifies the `file` to read from.\n"+
		"REQUIRED.\n")
	flag.StringVar(&inFile, "-input", "", "(see -i)")

	flag.StringVar(&outFile, "o", "", "Specifies the `file` to output to.\n"+
		"Defaults to STDOUT")
	flag.StringVar(&outFile, "-output", "", "(see -o)")

	flag.StringVar(&logfile, "l", "", "Specifies the `file` to log to.\n"+
		"Defaults to STDOUT")
	flag.StringVar(&logfile, "-logfile", "", "(see -l)")

	flag.StringVar(&ytKeyFile, "y", "", "Specifies the `file` to read the Youtube API key from.\n"+
		"Disables YouTube if not provided.")
	flag.StringVar(&ytKeyFile, "-youtubeKey", "", "(see -y)")

	flag.BoolVar(&all, "a", false, "Enables all mode.` `\n"+
		"Prints all links, not just borked ones.")
}

// main ########################################################################
func main() {
	var err error
	start := time.Now()

	var ytKey string
	if ytKey, err = handleArgs(); err != nil {
		fmt.Println(err)
		usage()
		return
	}

	log := generateLogger(logfile)

	scanner, out, err := openInOut(inFile, outFile)
	if err != nil {
		log.Print(err)
		return
	}
	defer out.Close()

	log.Print("Logger and I/O streams generated.")

	// if a key was given, generate a youtube parser.
	var yt *youtube.Client
	if ytKey != "" {
		log.Print("DEBUG: Found YouTube key: '" + ytKey + "'")
		yt, err = youtube.NewClient(ytKey)
		if err != nil {
			log.Print(err)
			return
		}
	} else {
		yt = nil
		log.Print("No YouTube key given; YouTube checks will be disabled.")
	}

	// generate a Parser
	parser := &parse.Parser{
		Scan: scanner,
		Out:  out}

	// parse the file, one goroutine/link
	var wg sync.WaitGroup
	links := parser.ParseFile() // returns a slice of Link POINTERS
	log.Print("File Parsed.")
	for i, link := range links {
		wg.Add(1)
		go link.Test(i, false, &wg, yt) // test the link
	}
	wg.Wait()

	// now that all threads are done, loop once more to print results
	fmt.Fprintf(out, "\n") // add a new line
	for _, link := range links {
		if all || link.StatusCode != 200 { // if all mode or not 200 OK, print.
			fmt.Fprintf(out, "%v\n"+
				"---> [%v]", link.Pprint(), link.StatusCode)
			// only print notes section if not empty
			for _, n := range link.Notes {
				fmt.Fprintf(out, "\n---> %v", n)
			}
			fmt.Fprintf(out, "\n\n") // add a new line
		}
	}

	fmt.Fprintf(out, "Program took %v.\n", time.Since(start))
}

// subroutines #################################################################

/* handleArgs
Parses flags and perpares fields based on the state of the flags.*/
func handleArgs() (string, error) {
	var ytKey string

	flag.Parse()

	// if not given, die
	if inFile == "" {
		return "", errors.New("input file was not specified")
	}

	// read youtube API key
	// slurp the file, cast result to string
	if ytKeyFile != "" {
		b, err := ioutil.ReadFile(ytKeyFile)
		if err != nil {
			return "", err
		}
		ytKey = strings.TrimSpace(string(b))
	}

	return ytKey, nil
}

/* generateLogger opens the named file for logging or sets the logger to STDOUT
if "" was given. */
func generateLogger(logfile string) *log.Logger {
	var file *os.File
	if logfile != "" {
		var err error
		file, err = os.OpenFile(logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			fmt.Printf("FATAL: Failed to open %s for logging.\n", logfile)
			os.Exit(0)
		}
	} else {
		file = os.Stdout
	}
	return log.New(file, "", log.Ltime)
}

/* openInOut
Dies if it fails to read inFile or create an outFile.
Return inFile, outFile */

func openInOut(inFile, outFile string) (*bufio.Scanner, *os.File, error) {
	var in *bufio.Scanner
	var out *os.File
	var err error

	// open in file
	inFd, err := os.Open(inFile)
	if err != nil {
		return nil, nil, errors.New("failed to open input file '" + inFile + "':" + err.Error())
	}
	in = bufio.NewScanner(inFd)

	// if output file was not given, print to STDOUT
	if outFile == "" {
		out = os.Stdout
	} else { // else, set up output file
		out, err = os.OpenFile(outFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			return nil, nil, errors.New("failed to open output file '" + inFile + "':" + err.Error())
		}
	}

	return in, out, nil
}

// util subroutines ############################################################

/* usage
Prints usage and dies. */
func usage() {
	fmt.Printf("Usage: %v [flags]\n Call with -h for help.\n", os.Args[0])
	os.Exit(0)
}
