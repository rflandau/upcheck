# UpCheck

[WebChecker](https://gitlab.com/rflandau/WebChecker), but in Go. Faster, lighter, and a fraction of the line count!

# Running
Compile with `cd src/ && go build upcheck.go` or grab and unpack a tarball in *releases*.

Run with `./upcheck -i <link_link>.ll -o <output file>`.

See below for the input file template

You can call with `-h` for all flags.

## Link Lists

Link lists are just text files following a certain format, like markdown.

`#` defines a section. `##` defines a subsection, but is optional. All following links will be noted as being under the most recent section and subsection.

See `template.linklist` for an example.

# TODO

*   Created dedicated, toggle-able Debug log